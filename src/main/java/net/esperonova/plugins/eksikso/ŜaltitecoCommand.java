package net.esperonova.plugins.eksikso;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ŜaltitecoCommand implements CommandExecutor {
	private final boolean value;

	public ŜaltitecoCommand(boolean value) {
		this.value = value;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(args.length != 0) return false;

        if(!(sender instanceof Player)) {
            sender.sendMessage("§cNur ludantoj povas uzi ĉi tiun komandon");
            return true;
        }

		EksiksoPlugin.setEnabledForPlayer((Player) sender, value);
		sender.sendMessage(
			"§aLa konvertado de la X-sistemo nun estas §l" + (value ? "§2ŝaltita" : "§4malŝaltita") + "§r§a."
		);
		return true;
	}
}
