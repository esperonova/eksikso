package net.esperonova.plugins.eksikso;

import java.io.File;
import java.io.IOException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class EksiksoPlugin extends JavaPlugin implements Listener {
	private static final boolean DEFAULT_ENABLED = true;

    private static FileConfiguration userConfig;
    private static File userConfigFile;

	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);

		getCommand("eksikson-ŝalti").setExecutor(new ŜaltitecoCommand(true));
		getCommand("eksikson-malŝalti").setExecutor(new ŜaltitecoCommand(false));

        userConfigFile = new File(getDataFolder(), "userConfig.yml");
        userConfig = YamlConfiguration.loadConfiguration(userConfigFile);
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		if(!isEnabledForPlayer(event.getPlayer())) return;
		event.setMessage(handleX(event.getMessage()));
	}

	@EventHandler
	public void onChangeSign(SignChangeEvent event) {
		if(!isEnabledForPlayer(event.getPlayer())) return;

		String[] originalLines = event.getLines();
		for(int i = 0; i < originalLines.length; i++) {
			event.setLine(i, handleX(originalLines[i]));
		}
	}

	@EventHandler
	public void onPreprocessCommand(PlayerCommandPreprocessEvent event) {
		String command = event.getMessage();
		if(
			(command.startsWith("/msg ") || command.startsWith("/w ") || command.startsWith("/tell ")) &&
				isEnabledForPlayer(event.getPlayer())
		) {
			int secondSpace = command.indexOf(' ', command.indexOf(' ') + 1);
			if(secondSpace != -1) {
				String rest = command.substring(secondSpace + 1);
				String newValue = command.substring(0, secondSpace + 1) + handleX(rest);
				event.setMessage(newValue);
			}
		}
	}

	private boolean isEnabledForPlayer(Player player) {
		var section = userConfig.getConfigurationSection(player.getUniqueId().toString());
		if(section == null) return DEFAULT_ENABLED;
		return section.getBoolean("enabled", DEFAULT_ENABLED);
	}

	public static void setEnabledForPlayer(Player player, boolean enabled) {
		var section = userConfig.getConfigurationSection(player.getUniqueId().toString());
		if(section == null) {
			section = userConfig.createSection(player.getUniqueId().toString());
		}
		section.set("enabled", enabled);
		saveUserConfig();
	}

	private static String handleX(String src) {
		if(src.length() < 2) return src;

		StringBuilder builder = new StringBuilder(src.length());
		char prev = 0;
		var iter = new StringCharacterIterator(src);
		prev = iter.first();

		char current = iter.next();
		while(current != CharacterIterator.DONE) {
			if(current == 'x' || current == 'X') {
				char result = 0;
				if(prev == 'c') result = 'ĉ';
				else if(prev == 'C') result = 'Ĉ';
				else if(prev == 'g') result = 'ĝ';
				else if(prev == 'G') result = 'Ĝ';
				else if(prev == 'h') result = 'ĥ';
				else if(prev == 'H') result = 'Ĥ';
				else if(prev == 'j') result = 'ĵ';
				else if(prev == 'J') result = 'Ĵ';
				else if(prev == 's') result = 'ŝ';
				else if(prev == 'S') result = 'Ŝ';
				else if(prev == 'u') result = 'ŭ';
				else if(prev == 'U') result = 'Ŭ';

				if(result == 0) {
					builder.append(prev);
					prev = current;
				}
				else {
					builder.append(result);
					prev = iter.next();
				}
			}
			else {
				builder.append(prev);
				prev = current;
			}
			current = iter.next();
		}
		if(prev != CharacterIterator.DONE) builder.append(prev);

		return builder.toString();
	}

	private static void saveUserConfig() {
        try {
            userConfig.save(userConfigFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
	}
}
